<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainersPokemonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainers__pokemons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pokemon');
            $table->integer('id_trainer');
            $table->boolean('favorite')->default(false);
            $table->timestamps();
            $table->foreign('id_trainer')->references('id')->on('trainers');
            $table->foreign('id_pokemon')->references('id')->on('pokemons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainers__pokemons');
    }
}
