<?php

use Illuminate\Database\Seeder;

class TrainersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainers = [
            ['firstname' => "Jonathan", 'lastname' => "Joestar"],
            ['firstname' => "Joseph", 'lastname' => "Joestar"],
            ['firstname' => "Jotaro", 'lastname' => "Kujo"],
            ['firstname' => "Josuke 4", 'lastname' => "Higashikata"],
            ['firstname' => "Giorno", 'lastname' => "Giovanna"],
            ['firstname' => "Jolyne", 'lastname' => "Kujo"],
            ['firstname' => "Jonnhy", 'lastname' => "Joestar"],
            ['firstname' => "Josuke 8", 'lastname' => "Higashikata"]
        ];
        foreach($trainers  as $jojo){
            DB::table('trainers')->insert([
                "firstname" => $jojo['firstname'],
                "lastname" => $jojo['lastname']
            ]);

        }
    }
}
