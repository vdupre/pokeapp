<?php

use Illuminate\Database\Seeder;

class TrainersPokemonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($trainer = 1; $trainer <= 8; $trainer++) {

            for ($pkmn = 1; $pkmn <= 6; $pkmn++) {
                DB::table('trainers__pokemons')->insert([
                    "id_trainer" => $trainer,
                    "id_pokemon" => random_int(1, 152),
                    "favorite" => ($pkmn == 1) ? 1 : 0
                ]);
            }
        }
    }
}
