<?php

use Illuminate\Database\Seeder;

class PokemonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($id = 1; $id < 152; $id++) {
            $poke = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $id));
            $pokename = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon-species/' . $id));
            DB::table('pokemons')->insert([
                "pokedex_id" => $id,
                "name" => $pokename->names[6]->name,
                "hp" => $poke->stats[5]->base_stat,
                "atk" => $poke->stats[4]->base_stat,
                "def" => $poke->stats[3]->base_stat,
                "spe" => $poke->stats[0]->base_stat
            ]);
        }
    }
}
