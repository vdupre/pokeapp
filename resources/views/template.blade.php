<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ env('APP_NAME') }}</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" />
    <!-- Materialize CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <ul id="dropdown_pkmn" class="dropdown-content">
        <li><a href="{{route('pokemons.index')}}">Pokémons</a></li>
        <li class="divider"></li>
        <li><a href="{{route('pokemons.create')}}">Nouveau pokémon</a></li>
    </ul>
    <ul id="dropdown_trainers" class="dropdown-content">
        <li><a href="{{route('trainers.index')}}">Dresseurs</a></li>
        <li class="divider"></li>
        <li><a href="{{route('trainers.create')}}">Nouveau dresseur</a></li>
    </ul>
    <ul id="dropdown_teams" class="dropdown-content">
        <li><a href="{{route('teams.index')}}">Equipes</a></li>
        <li class="divider"></li>
        <li><a href="{{route('teams.create')}}">Nouvelle equipe</a></li>
    </ul>
    <ul id="dropdown_fights" class="dropdown-content">
        <li><a href="{{route('fights.index')}}">Combats</a></li>
        <li class="divider"></li>
        <li><a href="{{route('fights.create')}}">Nouveau combat</a></li>
    </ul>

    <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container"><a id="logo-container" href="{{route('pokemons.index')}}" class="brand-logo">Pokéapp</a>
            <ul class="right hide-on-med-and-down">
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_pkmn">Pokémons<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_trainers">Dresseurs<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_teams">Equipes<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_fights">Combats<i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">
                <li><a href="{{route('pokemons.index')}}">Pokémons</a></li>
                <li><a href="{{route('pokemons.create')}}">Nouveau pokémon</a></li>
                <li class="divider"></li>
                <li><a href="{{route('trainers.index')}}">Dresseurs</a></li>
                <li><a href="{{route('trainers.create')}}">Nouveau dresseur</a></li>
                <li class="divider"></li>
                <li><a href="{{route('teams.index')}}">Equipes</a></li>
                <li><a href="{{route('teams.create')}}">Nouvelle équipe</a></li>
                <li class="divider"></li>
                <li><a href="{{route('fights.index')}}">Combats</a></li>
                <li><a href="{{route('fights.create')}}">Nouveau combat</a></li>

            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>

    <div class="container">
        @yield('main')
    </div>
    <script src="{{ asset('js/app.js') }}" type="text/js"></script>

    <!-- Materialize -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(document).ready(function() {
        
            $('.sidenav').sidenav();  
            $("li .dropdown-trigger").dropdown({
                'hover':true,
                'coverTrigger':false
            });
        });
    </script>
</body>

</html>