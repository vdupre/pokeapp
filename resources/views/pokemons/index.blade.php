@extends('template')

@section('main')
<div class="row">
    <div class="col s12">
        <div class="col s12">

            @if(session()->get('success'))
            <div class="card panel green darken-1">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>
        <h1 class="display-3">Pokémons</h1>

        <form action="{{ route('pokemons.create')}}" method="post">
            @csrf
            @method('GET')
            <button type="submit" class="waves-effect waves-light btn" id="add">Ajouter un pokémon</button>
        </form>

        <div class="crud-flexbox">
            @foreach($pokemons as $pokemon)


            <div class="card col s12 m4 l4 hoverable" id="{{$pokemon->id}}">
                <div class="card-image">
                    <img class="responsive-img col s6 offset-s3">
                </div>
                <div class="card-content">
                    <span class="card-title center-align">{{$pokemon->name}}</span>
                    <ul>
                        <li class="center-align">N° Pokedex : {{$pokemon->pokedex_id}}</li>
                        <li class="center-align">PV : {{$pokemon->hp}}</li>
                        <li class="center-align">Attaque : {{$pokemon->atk}}</li>
                        <li class="center-align">Défense : {{$pokemon->def}}</li>
                        <li class="center-align">Vitesse :{{$pokemon->spe}} </li>
                    </ul>
                </div>
                <div class="card-action">
                    <div class="col s6 center-align">
                        <a href="{{ route('pokemons.edit',$pokemon->id)}}" class="waves-effect waves-light btn"><i class="material-icons">create</i></a>
                    </div>
                    <div class="col s6 center-align">
                        <form action="{{ route('pokemons.destroy', $pokemon->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons">delete</i></button>
                        </form>
                    </div>

                </div>
            </div>

            @endforeach

        </div>

        {{ $pokemons->links('custom-pagination') }}
    </div>

</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems);
    });


    $(document).ready(function() {
        loadSprites($('.card:first'), 0);
    });

    function loadSprites(element, i) {
        var cards = $('.card');


        $.ajax({

            type: 'GET',

            url: "https://pokeapi.co/api/v2/pokemon/" + element.attr("id"),

            success: function(response) {
                element.children(".card-image").children().attr('src', response.sprites.front_default);
                i++;
                if (i <= cards.length) {
                    loadSprites($(cards[i]), i);
                }
                
            }
        });

    }
</script>
@endsection