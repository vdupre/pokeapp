@extends('template')

@section('main')
<div class="row">
    <div class="col s8 offset-s2">
        <h1 class="center-align">Ajouter un pokémon</h1>
        <div>
            @if ($errors->any())
            <div class="red darken-1">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('pokemons.store') }}">
                @csrf
                <div class="input-field col s12">
                    <label for="name">Nom</label>
                    <input type="text" class="form-control" name="name" />
                </div>

                <div class="input-field col s12 l6">
                    <label for="pokedex_id">N° Pokedex</label>
                    <input type="number" class="form-control" name="pokedex_id" />
                </div>

                <div class="input-field col s12 l6">
                    <label for="hp">Points de vie</label>
                    <input type="number" class="form-control" name="hp" />
                </div>
                
                <div class="input-field col s12 l4">
                    <label for="atk">Attaque</label>
                    <input type="number" class="form-control" name="atk" />
                </div>

                <div class="input-field col s12 l4">
                    <label for="def">Défense</label>
                    <input type="number" class="form-control" name="def" />
                </div>

                <div class="input-field col s12 l4">
                    <label for="spe">Vitesse</label>
                    <input type="number" class="form-control" name="spe" />
                </div>
                <div class="col l8 offset-l4">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer le pokémon
                        <i class="material-icons right">save</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection