@extends('template')

@section('main')
<div class="row">
    <div class="col s12">
        <div class="col s12">

            @if(session()->get('success'))
            <div class="card panel green darken-1">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>
        <h1 class="display-3">Combats</h1>

        <form action="{{ route('fights.create')}}" method="post">
            @csrf
            @method('GET')
            <button type="submit" class="waves-effect waves-light btn" id="add">Nouveau combat</button>
        </form>

        <div class="crud-flexbox" id="flex">
            @foreach($data["fights"] as $fight)
            <div class="card col s12 m4 l4 hoverable left">
                <div class="card-content">
                    <p class="col s12 m12 center-align">{{$data["trainers"][$fight->first_trainer_id - 1]->firstname}} {{$data["trainers"][$fight->first_trainer_id - 1]->lastname}}</p>
                    <p class="col s12 m12 center-align"> vs </p>
                    <p class="col s12 m12 center-align"> {{$data["trainers"][$fight->second_trainer_id - 1]->firstname}} {{$data["trainers"][$fight->second_trainer_id - 1]->lastname}}</p>
                </div>
                <div class="card-content">
                    <h2 class="card-title center-align col s12">Vainqueur :</h2>
                    <span class="card-title center-align col s12"> {{$data["trainers"][$fight->winner_id - 1]->firstname}} {{$data["trainers"][$fight->winner_id - 1]->lastname}} </span>
                </div>
                <div class="card-action">

                    <div class="col s12 center-align">
                        <form action="{{ route('fights.destroy', $fight->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons">delete</i></button>
                        </form>
                    </div>

                </div>

            </div>
            @endforeach
        </div>

        {{ $data["fights"]->links('custom-pagination') }}
    </div>

</div>

@endsection