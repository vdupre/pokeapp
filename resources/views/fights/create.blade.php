@extends('template')

@section('main')
<div class="row">
    <div class="col s8 offset-s2">
        <h1 class="center-align">Nouveau combat</h1>
        <div>
            @if ($errors->any())
            <div class="red darken-1">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('fights.store') }}">
                @csrf

                <div class="input-field col s12">
                    <select name="team1">
                        <option disabled selected>Sélectionner une équipe</option>
                        @foreach ($teams as $team)
                        <option value="{{$team->team_id}}">{{$team->trainer_firstname}} {{$team->trainer_lastname}} ({{$team->pokemon_name}})</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field col s12">
                    <select name="team2">
                        <option disabled selected>Sélectionner une équipe</option>
                        @foreach ($teams as $team)
                        <option value="{{$team->team_id}}">{{$team->trainer_firstname}} {{$team->trainer_lastname}} ({{$team->pokemon_name}})</option>
                        @endforeach
                    </select>
                </div>

                <div class="col l8 offset-l4">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Combattre !
                        <i class="material-icons right">save</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
@endsection