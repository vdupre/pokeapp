@extends('template')

@section('main')
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">

            @if(session()->get('success'))
            <div class="card panel green darken-1">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>
        <h1 class="display-3">Dresseurs</h1>

        <form action="{{ route('trainers.create')}}" method="post">
            @csrf
            @method('GET')
            <button type="submit" class="waves-effect waves-light btn" id="add">Ajouter un dresseur</button>
        </form>

        <div class="crud-flexbox" id="flex">
            @foreach($trainers as $trainer)
            <div class="card col s12 m4 l4 hoverable left">
                <div class="card-content">
                    <span class="card-title center-align"> {{$trainer->firstname}} {{$trainer->lastname}} </span>
                </div>
                <div class="card-action">
                    <div class="col s6 center-align">
                        <a href="{{ route('trainers.edit',$trainer->id)}}" class="waves-effect waves-light btn"><i class="material-icons">create</i></a>
                    </div>
                    <div class="col s6 center-align">
                        <form action="{{ route('trainers.destroy', $trainer->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons">delete</i></button>
                        </form>
                    </div>

                </div>

            </div>
            @endforeach
        </div>
        {{ $trainers->links('custom-pagination') }}
        <div>
        </div>
        @endsection