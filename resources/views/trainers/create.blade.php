@extends('template')

@section('main')
<div class="row">
    <div class="col s8 offset-s2">
        <h1 class="center-align">Ajouter un dresseur</h1>
        <div>
            @if ($errors->any())
            <div class="red darken-1">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('trainers.store') }}">
                @csrf
                <div class="input-field col s12">
                    <label for="firstname">Prénom</label>
                    <input type="text" class="form-control" name="firstname" />
                </div>

                <div class="input-field col s12">
                    <label for="lastname">Nom</label>
                    <input type="text" class="form-control" name="lastname" />
                </div>

                <div class="col l8 offset-l4">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer le dresseur
                        <i class="material-icons right">save</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection