@extends('template')
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Editer un dresseur</h1>

        @if ($errors->any())
        <div class="red darken-1">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('trainers.update', $trainer->id) }}">
            @method('PATCH')
            @csrf

            <div class="input-field col s12">
                <label for="firstname">Prénom</label>
                <input type="text" class="form-control" name="firstname" value="{{ $trainer->firstname }}"/>
            </div>

            <div class="input-field col s12">
                <label for="lastname">Nom</label>
                <input type="text" class="form-control" name="lastname"  value="{{ $trainer->lastname }}"/>
            </div>

            <div class="col l8 offset-l4">
                <button class="btn waves-effect waves-light" type="submit" name="action">Mettre à jour le dresseur
                    <i class="material-icons right">save</i>
                </button>
            </div>

        </form>
    </div>
</div>
@endsection