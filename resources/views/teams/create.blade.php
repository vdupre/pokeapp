@extends('template')

@section('main')
<div class="row">
    <div class="col s8 offset-s2">
        <h1 class="center-align">Ajouter un pokémon à une équipe</h1>
        <div>
            @if ($errors->any())
            <div class="red darken-1">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
            @endif
            <form method="post" action="{{ route('teams.store') }}">
                @csrf

                <div class="input-field col s12">
                    <select name="id_trainer">
                        <option disabled selected>Sélectionner un dresseur</option>
                        @foreach ($data["trainers"] as $trainer)
                        <option value="{{$trainer->id}}">{{$trainer->firstname}} {{$trainer->lastname}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field col s12">
                    <select name="id_pokemon">
                        <option disabled selected>Sélectionner un pokémon</option>
                        @foreach ($data["pokemons"] as $pokemon)
                        <option value="{{$pokemon->id}}">{{$pokemon->name}}</option>
                        @endforeach
                    </select>
                </div>

                <label for="is_favorite" >
                    <input type="checkbox" id="is_favorite" name="is_favorite" value="{{$trainer->id}}" />
                    <span>Pokemon favori</span>
                </label>

                <div class="col l8 offset-l4">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer l'équipe
                        <i class="material-icons right">save</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
@endsection