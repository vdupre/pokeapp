@extends('template')

@section('main')
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">

            @if(session()->get('success'))
            <div class="card panel green darken-1">
                {{ session()->get('success') }}
            </div>
            @endif
        </div>
        <h1 class="display-3">Equipes</h1>

        <form action="{{ route('teams.create')}}" method="post">
            @csrf
            @method('GET')
            <button type="submit" class="waves-effect waves-light btn" id="add">Ajouter un pokémon à une équipe</button>
        </form>

        <div class="crud-flexbox" id="teams">
            @foreach($teams as $team)
            <div class="card col s12 m4 l4 hoverable left">
                <div class="card-content">
                    <ul>
                        <li>Dresseur : {{$team->trainer_firstname}} {{$team->trainer_lastname}}</li>
                        <li>Pokemon : {{$team->pokemon_name}}</li>
                        <li>Favori : {{($team->favorite == 1)?"Oui":"Non"}}</li>
                    </ul>
                </div>
                <div class="card-action">
                    <div class="col s6 center-align">
                        <a href="{{ route('teams.edit',$team->id)}}" class="waves-effect waves-light btn"><i class="material-icons">create</i></a>
                    </div>
                    <div class="col s6 center-align">
                        <form action="{{ route('teams.destroy', $team->id)}}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="waves-effect waves-light btn red darken-1"><i class="material-icons">delete</i></button>
                        </form>
                    </div>

                </div>

            </div>
            @endforeach
        </div>
        {{ $teams->links('custom-pagination') }}
        <div>
        </div>
        @endsection