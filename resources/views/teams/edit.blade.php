@extends('template')
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Editer une équipe</h1>

        @if ($errors->any())
        <div class="red darken-1">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br />
        @endif
        <form method="post" action="{{ route('teams.update', $data['teams']->id) }}">
            @method('PATCH')
            @csrf

            <input type="hidden" name="id" value="{{$data['teams']->id}}"/>
            <div class="input-field col s12">
                    <select name="id_trainer">
                    <option value="{{$data['teams']->id_trainer}}" selected>{{$data['teams']->trainer_firstname}} {{$data['teams']->trainer_lastname}}</option>
                        @foreach ($data["trainers"] as $trainer)
                        <option value="{{$trainer->id}}">{{$trainer->firstname}} {{$trainer->lastname}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="input-field col s12">
                    <select name="id_pokemon">
                        <option value="{{$data['teams']->pokemon_id}}" selected>{{$data['teams']->pokemon_name}}</option>
                        @foreach ($data["pokemons"] as $pokemon)
                        <option value="{{$pokemon->id}}">{{$pokemon->name}}</option>
                        @endforeach
                    </select>
                </div>

                
                @if ($data["teams"]->favorite)
                <label for="is_favorite" >
                    <input type="checkbox" id="is_favorite" name="is_favorite" checked="ckecked" value="{{$trainer->id}}"/>
                    <span>Pokemon favori</span>
                </label>
                @else
                <label for="is_favorite" >
                    <input type="checkbox" id="is_favorite" name="is_favorite" value="{{$trainer->id}}"/>
                    <span>Pokemon favori</span>
                </label>
                @endif
                <div class="col l8 offset-l4">
                    <button class="btn waves-effect waves-light" type="submit" name="action">Modifier l'équipe
                        <i class="material-icons right">save</i>
                    </button>
                </div>

        </form>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('select').formSelect();
    });
</script>
@endsection