<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Fights extends Model
{
    protected $fillable = [
        'first_trainer_id',
        'second_trainer_id',
        'winner_id'
    ];
}
