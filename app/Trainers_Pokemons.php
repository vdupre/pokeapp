<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainers_Pokemons extends Model
{
    protected $fillable = [
        "id_pokemon",
        "id_trainer",
        "is_favorite"
    ];

}
