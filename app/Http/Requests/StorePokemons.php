<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePokemons extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:pokemons',
            'pokedex_id' => 'required|unique:pokemons',
            'hp' => 'required',
            'atk' => 'required',
            'def' => 'required',
            'spe' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Un nom de pokémon est requis',
            'name.unique'  => 'Ce nom de pokémon est déjà pris',
            'pokedex_id.required' => 'Un numéro de pokédex est requis',
            'pokedex_id.unique'  => 'Ce numéro de pokédex est déjà pris',
            'hp.required' => 'Un nombre de PV est requis',
            'atk.required' => 'La statistique d\'attaque est requise',
            'def.required' => 'La statistique de défense est requise',
            'spe.required' => 'La statistique de vitesse est requise',
        ];
    }
}
