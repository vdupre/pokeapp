<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;
use Illuminate\Support\Facades\DB;

class StoreTeams extends FormRequest
{
    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extend(
            'UniqueFavorite',
            function ($attribute, $value, $parameters) {
                $has_favorite = DB::table('trainers__pokemons')->where("id_trainer", "=", $value)->where("favorite", "=", "1")->count();
                if ($has_favorite == 1) {
                    return false;
                } else {
                    return true;
                }
            },
            'Ce dresseur possède déjà un pokémon favori'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_trainer' => 'required|exists:trainers,id',
            'id_pokemon' => 'required|exists:pokemons,id',
            'is_favorite' => 'UniqueFavorite'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id_trainer.required' => 'Un dresseur est requis',
            'id_trainer.exists'  => 'Ce dresseur n\'existe pas',
            'id_pokemon.required' => 'Un pokémon est requis',
            'id_pokemon.exists'  => 'Ce pokémon n\'existe pas',
        ];
    }
}
