<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTrainers;
use App\Trainers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TrainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainers = DB::table('trainers')->paginate(24);
        return view('trainers.index', compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('trainers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrainers $request)
    {
        $request->validated();

        $trainer = new Trainers([
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname')
        ]);
        $trainer->save();
        return redirect('/trainers')->with('success', 'Un nouveau dresseur fait son apparition !');
    }

     /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trainer = Trainers::find($id);
        return view('trainers.edit', compact('trainer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTrainers $request,$id)
    {
        $request->validated();

        $trainer = Trainers::find($id);
        $trainer->firstname =  $request->get('firstname');
        $trainer->lastname = $request->get('lastname');

        $trainer->save();

        return redirect('/trainers')->with('success', 'Dresseur mis à jour !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trainer = Trainers::find($id);
        $trainer->delete();

        return redirect('/trainers')->with('success', 'Dresseur supprimé !');
    }
}
