<?php

namespace App\Http\Controllers;

use App\Fights;
use App\Pokemons;
use App\Trainers_Pokemons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FightsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fights = DB::table('fights')->orderByDesc('id')->paginate(24);
        $trainers = DB::table('trainers')->get();
        $trainers = collect($trainers)->toArray(); 
        $data = [
            "fights" => $fights,
            "trainers" => $trainers
        ];
        return view('fights.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = DB::table('trainers__pokemons')
            ->join('trainers', 'trainers__pokemons.id_trainer', "=", "trainers.id")
            ->join('pokemons', 'trainers__pokemons.id_pokemon', '=', 'pokemons.id')
            ->select(
                'trainers__pokemons.id as team_id',
                'trainers.id as trainer_id',
                'trainers.firstname as trainer_firstname',
                'trainers.lastname as trainer_lastname',
                'pokemons.name as pokemon_name'
            )
            ->distinct('trainer_id')
            ->where('trainers__pokemons.favorite', '=', '1')
            ->get();

        return view('fights.create', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'team1' => 'required|exists:trainers__pokemons,id|different:team2',
            'team2' => 'required|exists:trainers__pokemons,id|different:team1',
        ]);

        $team1 = DB::table('trainers__pokemons')
            ->where('trainers__pokemons.id', '=', $request->get('team1'))
            ->first();

        $team2 = DB::table('trainers__pokemons')
            ->where('trainers__pokemons.id', '=', $request->get('team2'))
            ->first();

        $fight = new Fights([
            'first_trainer_id' => $team1->id_trainer,
            'second_trainer_id' => $team2->id_trainer,
            'winner_id' => $this->fight($team1, $team2),
        ]);
        $fight->save();
        return redirect('/fights')->with('success', 'Combat effectué !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fight = Fights::find($id);
        $fight->delete();

        return redirect('/fights')->with('success', 'Combat supprimé !');
    }

    public function spe_sort($a, $b)
    {
        if ($a->spe == $b->spe) {
            return 0;
        }
        return ($a->spe > $b->spe) ? -1 : 1;
    }

    public function fight($team1, $team2)
    {
        $pkmn_t1 = Pokemons::find($team1->id_pokemon);
        $pkmn_t2 = Pokemons::find($team2->id_pokemon);

        $pkmn_fight = [
            $team1->id_trainer => $pkmn_t1,
            $team2->id_trainer => $pkmn_t2
        ];

        uasort($pkmn_fight, array($this, "spe_sort"));
        $first_pkmn = array_keys($pkmn_fight)[0];
        $second_pkmn = array_keys($pkmn_fight)[1];

        $pkmn_win = $this->attack($pkmn_fight[$first_pkmn], $pkmn_fight[$second_pkmn]);
        
        $vainqueur = array_search($pkmn_win, $pkmn_fight);
            
        return $vainqueur;
    }


    public static function attack($atk_pkmn, $def_pkmn)
    {
        $damage = ((50 * 0.4 + 2) * rand(1, 100) * $atk_pkmn->atk) / ($def_pkmn->def * 50) + 2;
        if ($efficacité = rand(0, 100) < 5) {
            $damage = 0; //5% d'échec
        } elseif ($efficacité > 95) {
            $damage = $damage * 1.5; //5% de critique
        } else {
            $damage = $damage * (1 + $efficacité / 200); //Attaque normale
        }
        $def_pkmn->hp -= $damage;
        if ($def_pkmn->hp <= 0) {
            $this->attack($def_pkmn, $atk_pkmn);
        } else {
            return $atk_pkmn;
        }
    }
}
