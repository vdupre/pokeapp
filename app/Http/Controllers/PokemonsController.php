<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePokemons;
use App\Http\Requests\UpdatePokemons;
use App\Pokemons;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PokemonsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param string $sort = 'pokedex_id'
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pokemons = DB::table('pokemons')->paginate(24);
        return view('pokemons.index', compact('pokemons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pokemons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePokemons $request)
    {
        $request->validated();
        $pokemon = new Pokemons([
            'name' => $request->get('name'),
            'pokedex_id' => $request->get('pokedex_id'),
            'hp' => $request->get('hp'),
            'atk' => $request->get('atk'),
            'def' => $request->get('def'),
            'spe' => $request->get('spe'),
        ]);
        $pokemon->save();
        return redirect('/pokemons')->with('success', 'Un nouveau pokémon fait son apparition !');
    }

     /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pokemon = Pokemons::find($id);
        return view('pokemons.edit', compact('pokemon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePokemons $request,$id)
    {
        $request->validated($id);

        $pokemon = Pokemons::find($id);
        $pokemon->name = $request->get('name');
        $pokemon->pokedex_id = $request->get('pokedex_id');
        $pokemon->hp = $request->get('hp');
        $pokemon->atk = $request->get('atk');
        $pokemon->def = $request->get('def');
        $pokemon->spe = $request->get('spe');
        $pokemon->save();

        return redirect('/pokemons')->with('success', 'Pokemon mis à jour !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pokemon = Pokemons::find($id);
        $pokemon->delete();

        return redirect('/pokemons')->with('success', 'Pokémon supprimé !');
    }
}
