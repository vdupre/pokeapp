<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTeams;
use App\Http\Requests\UpdateTeams;
use App\Trainers_Pokemons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrainersPokemonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = DB::table('trainers__pokemons')
                ->join('trainers', 'trainers__pokemons.id_trainer', "=", "trainers.id")
                ->join('pokemons', 'trainers__pokemons.id_pokemon', '=', 'pokemons.id')
                ->select('trainers__pokemons.*', 'trainers.firstname as trainer_firstname', 'trainers.lastname as trainer_lastname', 
                'pokemons.name as pokemon_name', 'pokemons.id as pokemon_id')
                ->paginate(24);
        return view('teams.index', compact('teams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pokemons = DB::table('pokemons')->get(['name','id']);
        $trainers = DB::table('trainers')->get(['firstname', 'lastname','id']);
        $data = [
            "pokemons" => $pokemons,
            "trainers" => $trainers
        ];

        return view('teams.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeams $request)
    {
        $request->validated();
        $trainer_pokemon = new Trainers_Pokemons([
            'id_trainer' => $request->get('id_trainer'),
            'id_pokemon' => $request->get('id_pokemon'),
            'favorite' => ($request->get('is_favorite') != null)?1:0,
        ]);
        $trainer_pokemon->save();

        $pkmn = DB::table('pokemons')->select('name')->where('pokemon_id','=', $request->get('id_pokemon'))->get()[0];
        $trainer = DB::table('trainers')->select('firstname', 'lastname')->where('id','=', $request->get('id_trainer'))->get()[0];

        return redirect('/teams')->with('success', $pkmn->name .' rejoint l\'équipe de '. $trainer->firstname . ' '.$trainer->lastname .' !');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = DB::table('trainers__pokemons')
                ->join('trainers', 'trainers__pokemons.id_trainer', "=", "trainers.id")
                ->join('pokemons', 'trainers__pokemons.id_pokemon', '=', 'pokemons.id')
                ->select('trainers__pokemons.*', 'trainers.firstname as trainer_firstname', 'trainers.lastname as trainer_lastname', 
                'pokemons.name as pokemon_name', 'pokemons.id as pokemon_id')
                ->where('trainers__pokemons.id' , '=', $id)
                ->get()->first();

        $pokemons = DB::table('pokemons')->get(['name','id']);
        $trainers = DB::table('trainers')->get(['firstname', 'lastname','id']);

        $data = [
            "teams" => $teams,
            "pokemons" => $pokemons,
            "trainers" => $trainers
        ];
        
        return view('teams.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTeams $request,$id)
    {
        $request->validated();

        $pkmn_trainer = Trainers_Pokemons::find($id);
        $pkmn_trainer->id_trainer = $request->get('id_trainer');
        $pkmn_trainer->id_pokemon = $request->get('id_pokemon');
        $pkmn_trainer->favorite = ($request->get('is_favorite') != null)?1:0;
        

        $pkmn_trainer->save();

        return redirect('/teams')->with('success', 'Equipe mise à jour !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trainer_pkmn = Trainers_Pokemons::find($id);
        $team = DB::table('trainers__pokemons')
                ->join('trainers', 'trainers__pokemons.id_trainer', "=", "trainers.id")
                ->join('pokemons', 'trainers__pokemons.id_pokemon', '=', 'pokemons.id')
                ->select( 'trainers.firstname as trainer_firstname', 'trainers.lastname as trainer_lastname', 
                'pokemons.name as pokemon_name')
                ->where('trainers__pokemons.id' , '=', $id)
                ->get();
        $trainer_pkmn->delete();
        return redirect('/teams')->with('success', $team[0]->pokemon_name .' quitte l\'équipe de '. $team[0]->trainer_firstname .' '. $team[0]->trainer_lastname  .' !');
    }
}
