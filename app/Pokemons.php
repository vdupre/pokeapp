<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemons extends Model
{
    protected $fillable = [
        'name',
        'pokedex_id',
        'hp',
        'atk',
        'def',
        'spe',
    ];




}
