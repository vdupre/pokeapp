<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdatePokemons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pokemons:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads all pokemons on the PokeAPI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $last_pokemon = DB::table('pokemons')->count() + 1;
        $continue = true;
        do{
            try{
                $poke = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon/' . $last_pokemon));
                $pokename = json_decode(file_get_contents('https://pokeapi.co/api/v2/pokemon-species/' . $last_pokemon));
                DB::table('pokemons')->insert([
                    "pokedex_id" => $last_pokemon,
                    "name" => $pokename->names[6]->name,
                    "hp" => $poke->stats[5]->base_stat,
                    "atk" => $poke->stats[4]->base_stat,
                    "def" => $poke->stats[3]->base_stat,
                    "spe" => $poke->stats[0]->base_stat
                ]);
                $last_pokemon++;
            }catch(Exception $e){
                $poke = null;
            }
           

        }while($poke!=null);   
            
        
    }
}
