<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Pokemons;

Route::get('/', function () {
    return redirect('/pokemons');
});
Route::resource('pokemons', 'PokemonsController');
Route::resource('fights', 'FightsController');
Route::resource('trainers', 'TrainersController');
Route::resource('teams', 'TrainersPokemonsController');
